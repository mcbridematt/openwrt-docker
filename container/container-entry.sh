#!/bin/sh
rm -rf /tmp/*
if [ ! -z "${NETWORKWAIT}" ]; then
        while [ ! -f /tmp/.nsnetsetup ]; do
                sleep 1
        done
fi

for intf in $(ls /sys/class/net | grep eth); do
  ip addr flush dev "${intf}"
done
iptables --flush
iptables -t nat --flush
iptables -t mangle --flush
exec /sbin/init "$@"
